﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ParticleSystem : MonoBehaviour
{

    public GameObject particle;
    private List<Particle> particles = new List<Particle>();
    private Vector3 startPosition = new Vector3(0f, 0f, 0f);
    public int maxParticlesNumber = 1000;

    void Start()
    {
        //Stvaranje početnog skupa čestica
        int numP = (int)Randomizer.NextUniform(0,200);
        for (int i = 0; i < numP; ++i)
        {
            Particle cestica = new Particle(startPosition, instantiate());
            particles.Add(cestica);  
        }
        
    }

    //Osvježavanje stanja svake čestice i stvaranje novih
    void Update()
    {

         //Uništenje čestica kojima je isteklo vrijeme života
         List<Particle> temp = new List<Particle>(particles);
         foreach(Particle p in temp)
         {
               if(p.update(Time.deltaTime))
                {
                    Destroy(p.objekt);
                    particles.Remove(p);
                    
                }

            }

        //Stvaranje novih čestica
        int nextP = (int)Randomizer.NextUniform(0, 5);
        for (int i = 0; i < nextP; ++i)
        {
            if(particles.Count >= maxParticlesNumber)
            {
                break;
            }
            Particle cestica = new Particle(startPosition, instantiate());
            particles.Add(cestica);
        }
        print(particles.Count);
     



    }

    //Instanciranje objekta u sceni
    public GameObject instantiate()
    {
        GameObject obj = Instantiate(particle);
        obj.transform.SetParent(this.transform, false);
        return obj;

    }

    public void destroy(GameObject particle)
    {
        List<Particle> temp = new List<Particle>(particles);
        foreach (Particle p in temp)
        {
          if(p.objekt == particle)
            {
                Destroy(p.objekt);
                particles.Remove(p);
                return;
            }
          

        }
    }

  

   
}
