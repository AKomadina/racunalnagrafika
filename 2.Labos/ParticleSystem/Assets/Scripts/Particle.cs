﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle
{
    Vector3 position;
    Vector3 velocity;
    Vector3 size;
    float life;
    float time = 0f;
    public GameObject objekt;

    //Parametri sila
    Vector3 gravity = new Vector3(0, -9.81f, 0);
    float resist = 0.05f;

    //Parametri distribucija
    float mSize = 0.75f;
    float stdSize = 0.2f;
    float mLife = 7f;
    float stdLife = 2f;
    float mVel = 0f;
    float stdVel = 2f;
    float mVelY = -2f;
    float stdVelY = 0.5f;

    public Particle(Vector3 startPos, GameObject particle)
    {
        objekt = particle;

        //Određivanje početne pozicije čestice
        position = startPos;
        float startPosx = Randomizer.NextUniform(-50f, 50f);
        float startPosz = Randomizer.NextUniform(-50f, 50f);
        position.x = startPosx;
        position.z = startPosz;
        objekt.transform.localPosition = position;

        //Određivanje početne veličine čestice
        float startSize = Randomizer.NextGaussian(mSize, stdSize);
        size = new Vector3(startSize, startSize, startSize);
        objekt.transform.localScale = size;

        //Određivanje vremena života čestice
        life = Randomizer.NextGaussian(mLife, stdLife);

        //Početna brzina čestice
        float startVelocityX = Randomizer.NextGaussian(mVel, stdVel);
        float startVelocityY = Randomizer.NextGaussian(mVel, stdVel);
        float startVelocityZ = Randomizer.NextGaussian(mVelY, stdVelY);
        velocity += new Vector3(startVelocityX, startVelocityY, startVelocityZ);

        


    }

    public bool update(float dt)
    {
        //Povećanje trajanja
        time += dt;

        if (time >= life)
        {
            
            return true;
        }
        else
        {
            if (size.x <= 0.0001f)
            {
                return true;
            }

            //Smanjenje veličine
            size -= new Vector3(0.05f*dt, 0.05f*dt, 0.05f*dt);
            objekt.transform.localScale = size;

            //Izračun gravitacijske sile
            Vector3 Fg = new Vector3(size.x*gravity.x , size.y*gravity.y, size.z*gravity.z);

            //Izračun sile otpora zraka
            Vector3 Fr = new Vector3(0, velocity.y*velocity.y*size.y * size.y * resist, 0);

            //Izračun ukupne akceleracije
            Vector3 Fuk = Fg + Fr;
            Vector3 a = new Vector3(Fuk.x/size.x, Fuk.y/size.y, Fuk.z/size.z);

            //Povećanje brzine u ovisnosti o djelovanju sila
            velocity = velocity + a*dt;

            //Pomak čestice u ovisnoti o brzini
            position =position + velocity * dt;
            objekt.transform.localPosition = position;

            return false;

        }
       


    }

}

