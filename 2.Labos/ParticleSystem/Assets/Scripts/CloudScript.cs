﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        ManualLookAt(Camera.main.transform);

    }
    void ManualLookAt(Transform target)
    {

        Vector3 viewForward = new Vector3(0, 0, 0);
        Vector3 viewUp = new Vector3(0, 0, 0);
        Vector3 viewRight = new Vector3(0, 0, 0);

        //Vektor prema naprijed = pozicija kamera - pozicija objekta
        viewForward = target.position - transform.position;
        viewForward.Normalize();

        //Vektor prema gore = vektor kamere prema gore
        viewUp = Camera.main.transform.up;
        viewUp.Normalize();

        // Vektor prema desno = vektorski produkt dobivena 2 vektora
        viewRight = Vector3.Cross(viewUp, viewForward);

        transform.right = new Vector3(viewRight.x, viewRight.y, viewRight.z);
        transform.up = new Vector3(viewUp.x, viewUp.y, viewUp.z);
        transform.forward = new Vector3(viewForward.x, viewForward.y, viewForward.z);

        //transform.rotation = Quaternion.LookRotation(-Camera.main.transform.forward, Camera.main.transform.up);


    }
}
