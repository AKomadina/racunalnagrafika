﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class MainMenuScript : MonoBehaviour
{

    public GameObject menuCanvas;


    public void PlayGame()
    {
        menuCanvas.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
