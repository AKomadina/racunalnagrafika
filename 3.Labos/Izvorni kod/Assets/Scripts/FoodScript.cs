﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodScript : MonoBehaviour
{

    private List<GameObject> prateci = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name[0].Equals('P') || col.gameObject.name[0].Equals('Z'))
        {

        }
        else
        {
            if (col.GetType() == typeof(CapsuleCollider))
            {

                GameObject emitter = GameObject.Find("Emitter");
                emitter.GetComponent<GenetskiAlgoritam>().eat(col.gameObject, gameObject, prateci);
                Destroy(gameObject);
            }
            else if (col.GetType() == typeof(SphereCollider))
            {
                GameObject emitter = GameObject.Find("Emitter");
                emitter.GetComponent<GenetskiAlgoritam>().straight(col.gameObject, true);
                prateci.Add(col.gameObject);

                var targetPoint = new Vector3(transform.position.x, col.gameObject.transform.position.y, transform.position.z) - col.gameObject.transform.position;
                var targetRotation = Quaternion.LookRotation(targetPoint, Vector3.up);
                col.gameObject.transform.rotation = targetRotation;


               

            }
        }

    }

}
