﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


public class GenetskiAlgoritam : MonoBehaviour
{

    public GameObject predatorObject;
    public GameObject plainObject;
    public GameObject jedinkaObject;
    public GameObject foodObject;
    private GameObject brIterText;
    private GameObject brJedText;
    private GameObject najboljaText;
    private GameObject statistikaText;
    public string fileName;
    private string output;

    private List<Jedinka> populacija = new List<Jedinka>();
    private List<Plain> zrtve = new List<Plain>();
    private List<Predator> predatori = new List<Predator>();
    private List<GameObject> foodList = new List<GameObject>();
 

    private Vector3 startPosition = new Vector3(0f, 0f, 0f);
    public int populationSize = 100;

    [Range(0, 200)]
    public int foodCount = 100;
    [Range(0, 20)]
    public float totalEnergy = 10f;
    [Range(0, 30)]
    public float interval = 10f;

    [Range(0, 20)]
    public float plainCount = 10f;

    [Range(0, 10)]
    public float predatorCount = 6f;

    public int iteracija = 1;
    private int id = 0;
    public float time = 0;


    public void AdjustFoodCount(float newValue)
    {
        this.foodCount = (int)newValue;
    }

    public void AdjustTotalEnergy(float newValue)
    {
        this.totalEnergy = newValue;
    }

    public void AdjustInterval(float newValue)
    {
        this.interval = newValue;
    }

    void Start()
    {
        brIterText = GameObject.Find("BrojIteracija");
        brJedText = GameObject.Find("BrojJedinki");
        najboljaText = GameObject.Find("NajboljaJedinka");
        brIterText.GetComponent<UnityEngine.UI.Text>().text = "Dan: "+iteracija.ToString();
       
        for (int i = 0; i < populationSize/4; ++i)
        {
            
            Jedinka jed = new Jedinka(startPosition, instantiate(),totalEnergy, id);
            jed.housing = 1;
            jed.steering = 0;
            jed.setColor();
            populacija.Add(jed);  
        }
        
        for (int i = 0; i < populationSize / 4; ++i)
        {
            
            Jedinka jed = new Jedinka(startPosition, instantiate(), totalEnergy, id);
            jed.housing = 0;
            jed.steering = 1;
            jed.setColor();
            populacija.Add(jed);
        }
        for (int i = 0; i < populationSize / 4; ++i)
        {
            
            Jedinka jed = new Jedinka(startPosition, instantiate(), totalEnergy, id);
            jed.housing = 0;
            jed.steering = 0;
            jed.setColor();
            populacija.Add(jed);
        }
        for (int i = 0; i < populationSize / 4; ++i)
        {
            
            Jedinka jed = new Jedinka(startPosition, instantiate(), totalEnergy, id);
            jed.housing = 1;
            jed.steering = 1;
            jed.setColor();
            populacija.Add(jed);
        }
        
        for (int i = 0; i < predatorCount; ++i)
        {
            Predator pred = new Predator(startPosition, instantiatePredator(), totalEnergy, id);
            predatori.Add(pred);

        }

        createPlains();
        createFood();
        int broj = populacija.Count + predatori.Count + zrtve.Count;
        brJedText.GetComponent<UnityEngine.UI.Text>().text = "Broj jedinki: " + broj.ToString();
        Jedinka najbolja = najboljaJednika();
        najboljaText.GetComponent<UnityEngine.UI.Text>().text = "Brzina: " + System.Math.Round(najbolja.velocity, 3) +"\n"+ "Veličina: "+ najbolja.size.x + "\n" + "Boja: " + najbolja.tip + "\n" + "Životni vijek: " + najbolja.time ;
        countJedinke();
        statistika();
        
        write();

    }

    void Update()
    {
        if(time == 0)
        {
            
            time = time + Time.deltaTime;
            List<Jedinka> temp = new List<Jedinka>(populacija);
            foreach (Jedinka jed in temp)
            {
                jed.stuck = true;
                jed.update(Time.deltaTime);
            }
        }
        else
        {
            if (time >= interval)
            {
               
                ubij();
                deleteFood();
                createFood();
                createPlains();
                time = 0;
                iteracija += 1;
                int broj = populacija.Count + predatori.Count + zrtve.Count;
                brIterText.GetComponent<UnityEngine.UI.Text>().text = "Dan: " + iteracija.ToString();
                brJedText.GetComponent<UnityEngine.UI.Text>().text = "Broj jedinki: " + broj.ToString();
                Jedinka najbolja = najboljaJednika();
                najboljaText.GetComponent<UnityEngine.UI.Text>().text = "Brzina: " + System.Math.Round(najbolja.velocity, 3) + "\n" + "Veličina: " + najbolja.size.x + "\n" + "Boja: " + najbolja.tip + "\n" + "Životni vijek: " + najbolja.time;
                countJedinke();
                statistika();
                
                write();
                

            }
            else
            {

                time = time + Time.deltaTime;
                List<Jedinka> temp = new List<Jedinka>(populacija);
                foreach (Jedinka jed in temp)
                {
                    jed.update(Time.deltaTime);
                   

                }
                List<Predator> temp2 = new List<Predator>(predatori);
                foreach (Predator pred in temp2)
                {
                    pred.update(Time.deltaTime);


                }
                List<Plain> temp3 = new List<Plain>(zrtve);
                foreach (Plain pl in temp3)
                {
                    pl.update(Time.deltaTime);


                }

            }
        }
        
        

    }

    public GameObject instantiate()
    {
        id++;
        GameObject obj = Instantiate(jedinkaObject);
        obj.transform.SetParent(this.transform, false);
        return obj;

    }

    public GameObject instantiatePredator()
    {
        id++;
        GameObject obj = Instantiate(predatorObject);
        obj.transform.SetParent(this.transform, false);
        return obj;

    }

    public GameObject instantiatePlain()
    {
        id++;
        GameObject obj = Instantiate(plainObject);
        obj.transform.SetParent(this.transform, false);
        return obj;

    }

    public void replicate(Jedinka jed)
    {
        id += 1;
        Jedinka replica = new Jedinka(jed.position, instantiate(),totalEnergy, id);
        populacija.Add(replica);
    }

   

    public void eat(GameObject jedinka, GameObject food, List<GameObject> prateci)
    {
        List<Jedinka> temp = new List<Jedinka>(populacija);
        foreach (Jedinka j in temp)
        {
            if (j.objekt == jedinka)
            {
                j.incFoodCount();
                if(j.foodSense == true)
                {
                    j.foodSense = false;
                }
            }
            if (prateci.Contains(j.objekt))
            {
                j.foodSense = false;
            }
        }
        foodList.Remove(food);


    }

    public void eatPredator(GameObject jedinka, GameObject predator)
    {
        List<Jedinka> temp = new List<Jedinka>(populacija);
        foreach (Jedinka j in temp)
        {
            if (j.objekt == jedinka)
            {
                if (jedinka.transform.localPosition.x <= -10 || jedinka.transform.localPosition.x >= 10 || jedinka.transform.localPosition.z <= -10 || jedinka.transform.localPosition.z >= 10)
                {
                   
                }
              
                else
                {
                    populacija.Remove(j);
                    Destroy(j.objekt);
                }
                
            }

            
        }
        List<Predator> temp2 = new List<Predator>(predatori);
        foreach (Predator p in temp2)
        {
            if (p.objekt == predator)
            {
                p.energy += 50f;
            }
           

        }


    }


    public void eatPlain(GameObject jedinka, GameObject predator)
    {
        List<Plain> temp = new List<Plain>(zrtve);
        foreach (Plain pl in temp)
        {
            if (pl.objekt == jedinka)
            {
                
                
                    zrtve.Remove(pl);
                    Destroy(pl.objekt);
              

            }


        }
        List<Predator> temp2 = new List<Predator>(predatori);
        foreach (Predator p in temp2)
        {
            if (p.objekt == predator)
            {
                p.energy += 50f;
            }


        }


    }

    public void createPlains()
    {
        for (int i = 0; i < plainCount; ++i)
        {

            Plain pl = new Plain(startPosition, instantiatePlain(), id);
            zrtve.Add(pl);

        }


    }

    public void createFood()
    {
        for(int i = 0;i <foodCount; ++i)
        {
            
            float posX = Randomizer.NextUniform(-10f, 10f);
            float posZ = Randomizer.NextUniform(-10f, 10f);
            GameObject obj = Instantiate(foodObject);
            obj.transform.SetParent(this.transform, false);
            obj.name = "Food";
            Vector3 foodPos = new Vector3(posX, obj.transform.localPosition.y, posZ);
            obj.transform.localPosition = foodPos;
            foodList.Add(obj);

        }
        

    }
    public void deleteFood()
    {
        List<GameObject> temp = new List<GameObject>(foodList);
        foreach (GameObject food in temp)
        {
            foodList.Remove(food);
            Destroy(food);
        }
        


    }

    public void ubij()
    {
        List<Jedinka> temp = new List<Jedinka>(populacija);
        foreach (Jedinka jed in temp)
        {
            if (jed.energy <= 0 || jed.home() == false || jed.foodCount == 0)
            {
                populacija.Remove(jed);
                Destroy(jed.objekt);
            }
            else
            {
                jed.energy = totalEnergy;
                if (jed.foodCount == 1)
                {
                    jed.decFoodCount();
                    jed.returningHome = false;

                }
                
                if (jed.foodCount >= 2)
                {
                    jed.decFoodCount();
                    jed.returningHome = false;
                    if (jed.housing == 0)
                    {
                        id += 1;
                        jed.decFoodCount();
                        Jedinka newJed = new Jedinka(jed, instantiate(), totalEnergy, id);
                        populacija.Add(newJed);

                    }
                    else if(jed.housing == 1)
                    {
                        while(jed.foodCount > 0)
                        {
                            id += 1;
                            jed.decFoodCount();
                            Jedinka newJed = new Jedinka(jed, instantiate(), totalEnergy, id);
                            populacija.Add(newJed);
                        }
                    }
                }

             }
                
         }
            
        
    }

    public Jedinka najboljaJednika()
    {
        List<Jedinka> temp = new List<Jedinka>(populacija);
        Jedinka najbolja = populacija[0];
        float maxTime = 0;
        foreach(Jedinka jed in temp)
        {
            if(jed.time >= maxTime)
            {
                maxTime = jed.time;
                najbolja = jed;
            }
            
        }
        return najbolja;
    }

    public void statistika()
    {
        float sumSpeed = 0;
        float sumSize = 0;
        
        foreach(Jedinka jed in populacija)
        {
            sumSpeed += jed.velocity;
            sumSize += jed.size.x;
            

        }

        float avgSpeed = sumSpeed / populacija.Count;
        float avgSize = sumSize / populacija.Count;

        output += avgSpeed + "," + avgSize + "\n";
        statistikaText = GameObject.Find("Statistika");
        statistikaText.GetComponent<UnityEngine.UI.Text>().text = "Brzina: " + System.Math.Round(avgSpeed, 3)+"\n" + "Veličina: "+ System.Math.Round(avgSize, 3);

    }

    public void write()
    {
        var sr = File.CreateText(fileName);
        sr.WriteLine(output);
        sr.Close();
    }

    public void countJedinke()
    {
        List<int> counter = new List<int>();
        counter.Add(0);
        counter.Add(0);
        counter.Add(0);
        counter.Add(0);

        foreach (Jedinka jed in populacija)
        {
            if(jed.tip == "Zelena")
            {
                counter[0] += 1;
            }
            if (jed.tip == "Plava")
            {
                counter[1] += 1;
            }
            if (jed.tip == "Crvena")
            {
                counter[2] += 1;
            }
            if (jed.tip == "Magenta")
            {
                counter[3] += 1;
            }
        }

        int broj = populacija.Count + predatori.Count + zrtve.Count;
        output += broj + ";" + counter[0] + "," + counter[1] + "," + counter[2] + "," + counter[3] +","+predatori.Count+","+zrtve.Count+",";


    }

    

    public void straight(GameObject jedinka, bool flag)
    {
        List<Jedinka> temp = new List<Jedinka>(populacija);
        foreach (Jedinka j in temp)
        {
            if (j.objekt == jedinka)
            {
                if(j.returningHome == false)
                {
                    j.foodSense = flag;
                }
                
            }
        }
     


    }

    




}
