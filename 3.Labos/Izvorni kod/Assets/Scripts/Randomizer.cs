﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Randomizer
{
    public static float NextGaussian(float mean, float stdDev)
    {
        float rand1 = Random.Range(0.0f, 1.0f);
        float rand2 = Random.Range(0.0f, 1.0f);

        float n = Mathf.Sqrt(-2.0f * Mathf.Log(rand1)) * Mathf.Cos((2.0f * Mathf.PI) * rand2);

        return (mean + stdDev * n);
    }

    public static float NextUniform(float low, float high)
    {

        double value = Random.Range(low, high);
        return (float)value;

    }
}
