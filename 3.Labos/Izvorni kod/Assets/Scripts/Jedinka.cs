﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jedinka
{
    public int id;
    public Vector3 position;
    public float velocity;
    public Vector3 size;

    public float time = 0f;
    public GameObject objekt;
    public float energy;

    float mVelocity = 5f;
    float stdVelocity = 1.5f;
    float mSize = 0.1f;
    float stdSize = 0.02f;
    float turningRate = 0.5f;
    float mutationRate = 0.2f;
    float steeringMutation = 0.05f;
    float housingMutation = 0.05f;

    public int foodCount = 0;
    public bool stuck = false;
    public bool returningHome = false;

    public int housing = 1;
    public int steering = 0;
    public string tip;
    public bool foodSense = false;

    public void incFoodCount()
    {
        foodCount += 1;    
    }

    public void decFoodCount()
    {
        foodCount -= 1;
    }
   

    public Jedinka(Vector3 startPos, GameObject obj, float E ,int iden)
    {
        objekt = obj;

        position = startPos;
        
        float startPosx = Randomizer.NextUniform(-10f, 10f);
        float startPosz = Randomizer.NextUniform(-10f, 10f);
        position.x = startPosx;
        position.z = startPosz;  
        objekt.transform.localPosition = position;

        size = new Vector3(mSize, mSize, mSize);
        objekt.transform.localScale = size; 

        energy = E;
        velocity = mVelocity;

        objekt.name = iden.ToString();
        id = iden;
    }

    public Jedinka(Jedinka roditelj, GameObject obj, float E, int iden )
    {
        objekt = obj;

        position = roditelj.position;
        objekt.transform.localPosition = position;

        float mutationSize = Randomizer.NextGaussian(roditelj.size.x, 0.02f);
        size = new Vector3(mutationSize, mutationSize, mutationSize);
        objekt.transform.localScale = size;
        velocity = Randomizer.NextGaussian(roditelj.velocity, 1f);

        obj.GetComponent<SphereCollider>().radius = 90 * mutationSize;
        obj.GetComponent<SphereCollider>().center = new Vector3(0, 2 * mutationSize, 110 * mutationSize);

        energy = E;

        objekt.name = iden.ToString();
        id = iden;

        

        if (Random.value < steeringMutation)
        {
            steering = 1-roditelj.steering;
        }
        else
        {
            steering = roditelj.steering;
        }
        if (Random.value < housingMutation)
        {
            housing = 1-roditelj.housing;
        }
        else
        {
            housing = roditelj.housing;
        }

        setColor();

        


    }

    public void update(float dt)
    {

        time += dt;
        
        if(energy > 0)
        {
            if (objekt.transform.localPosition.x > -10 && objekt.transform.localPosition.x < 10 && objekt.transform.localPosition.z > -10 && objekt.transform.localPosition.z < 10)
            {

                if(foodCount >= 1 && returningHome == false && steering == 1 && foodSense == false)
                {

                    returnHome();
                    position = position + objekt.transform.forward*velocity*dt;
                    energy -= 2*(velocity) * (velocity) * (size.x) * (size.y) * (size.z);
                    objekt.transform.localPosition = position;
                    returningHome = true;


                }
                else
                {
                    if(returningHome == false && foodSense == false)
                    {
                        float rand = Random.Range(-1f, 1f);
                        float rot = rand * 35f;
                        if (Random.value < turningRate)
                        {
                            objekt.transform.Rotate(new Vector3(0, rot, 0), Space.World);

                        }
                    }                 

                    position = position + objekt.transform.forward * velocity * dt;
                    objekt.transform.localPosition = position;

                    energy -= 2*(velocity) * (velocity) * (size.x) * (size.y) * (size.z);

                }

                


            }
            else
            {
                if (foodCount < 1 && stuck == false)
                {
                    stuck = true;
                }

            }
            if (stuck == true)
            {
                if (objekt.transform.localPosition.x <= -10)
                {
                    objekt.transform.localRotation = Quaternion.Euler(0, 90, 0);
                    position.x = -10;
                }
                if (objekt.transform.localPosition.x >= 10)
                {
                    objekt.transform.localRotation = Quaternion.Euler(0, 270, 0);
                    position.x = 10;
                }
                if (objekt.transform.localPosition.z <= -10)
                {
                    objekt.transform.localRotation = Quaternion.Euler(0, 0, 0);
                    position.z = -10;
                }
                if (objekt.transform.localPosition.z >= 10)
                {
                    objekt.transform.localRotation = Quaternion.Euler(0, 180, 0);
                    position.z = 10;
                }
                objekt.transform.localPosition = position;


                position = position + objekt.transform.forward;
                objekt.transform.localPosition = position;

                stuck = false;
            }
            
        }
        else
        {
          
        }
        
        
     
       


    }

    public bool home()
    {
        if (objekt.transform.localPosition.x > -10 && objekt.transform.localPosition.x < 10 && objekt.transform.localPosition.z > -10 && objekt.transform.localPosition.z < 10)
        {
            return false;
        }
        else
        {
            return true;
        }
        
    }

    public void returnHome()
    {
        float distxPos = Mathf.Abs(position.x - 10);
        float distxNeg = Mathf.Abs(position.x + 10);
        float distzPos = Mathf.Abs(position.z - 10);
        float distzNeg = Mathf.Abs(position.z + 10);
        
        List<float> dist = new List<float>();
        dist.Add(distxPos);
        dist.Add(distxNeg);
        dist.Add(distzPos);
        dist.Add(distzNeg);


        int index = 0;
        float minDistance = dist[0];

        for (int i = 0; i < dist.Count ; i++)
        {
            
            if (dist[i] < minDistance)
            {
                minDistance = dist[i];
                index = i;
            }
        }

        if(index == 0)
        {
            objekt.transform.localRotation = Quaternion.Euler(0, 90, 0);
        }
        else if (index == 1)
        {
            objekt.transform.localRotation = Quaternion.Euler(0, 270, 0);
        }
        else if (index == 2)
        {
            objekt.transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
        else if (index == 3)
        {
            objekt.transform.localRotation = Quaternion.Euler(0, 180, 0);
        }


    }

    public void setColor()
    {
        var renderer = objekt.transform.GetChild(0).GetComponent<Renderer>();
        Color c;
        c = new Color(0f / 255, 0f / 255, 0f / 255);
        float colorNum = (velocity - 3) * 255 / 4;
        if (steering == 0 && housing == 1)
        {
            
            c = new Color(0f / 255, colorNum / 255, 0f / 255);
            if (velocity < 3)
            {
                c = new Color(0f / 255, 0f / 255, 0f / 255);
            }
            else if (velocity > 7)
            {
                c = new Color(0f / 255, 255f / 255, 0f / 255);
            }
            tip = "Zelena";
            
        }
        else if(steering == 1 && housing == 0)
        {
            c = new Color(colorNum / 255, 0f / 255, 0f / 255);
            if (velocity < 3)
            {
                c = new Color(0f / 255, 0f / 255, 0f / 255);
            }
            else if (velocity > 7)
            {
                c = new Color(255f / 255, 0f / 255, 0f / 255);
            }
            tip = "Crvena";
        }
        else if (steering == 0 && housing == 0)
        {
            c = new Color( 0f/ 255, 0f / 255, colorNum / 255);
            if (velocity < 3)
            {
                c = new Color(0f / 255, 0f / 255, 0f / 255);
            }
            else if (velocity > 7)
            {
                c = new Color(0f / 255, 0f / 255, 255f / 255);
            }
            tip = "Plava";
        }
        else if (steering == 1 && housing == 1)
        {
            c = new Color(colorNum / 255, 0f / 255, colorNum / 255);
            if (velocity < 3)
            {
                c = new Color(0f / 255, 0f / 255, 0f / 255);
            }
            else if (velocity > 7)
            {
                c = new Color(255f / 255, 0f / 255, 255f / 255);

            }
            tip = "Magenta";
        }


        renderer.material.SetColor("_Color", c);

    }

}

