﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredatorScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name[0].Equals('Z'))
        {
            GameObject emitter = GameObject.Find("Emitter");
            emitter.GetComponent<GenetskiAlgoritam>().eatPlain(col.gameObject, gameObject);

        }

        if (col.gameObject.name != "Field" && col.gameObject.name != "Home" && col.gameObject.name != "Food" && !col.gameObject.name[0].Equals('P'))
        {

            
                if (col.GetType() == typeof(CapsuleCollider))
                {

                    GameObject emitter = GameObject.Find("Emitter");
                    emitter.GetComponent<GenetskiAlgoritam>().eatPredator(col.gameObject, gameObject) ;


                }
                        


        }

    }


}
