﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plain
{
    public int id;
    public Vector3 position;
    public float velocity;
    public Vector3 size;

    public float time = 0f;
    public GameObject objekt;


    float mVelocity = 7f;
    float stdVelocity = 2f;
    float mSize = 0.05f;
    float stdSize = 0.003f;
    float turningRate = 0.2f;



    public Plain(Vector3 startPos, GameObject obj, int iden)
    {
        objekt = obj;

        position = startPos;

        float startPosx = Randomizer.NextUniform(-8f, 8f);
        float startPosz = Randomizer.NextUniform(-8f, 8f);
        position.x = startPosx;
        position.z = startPosz;
        objekt.transform.localPosition = position;

        float newSize = Randomizer.NextGaussian(mSize, stdSize);
        size = new Vector3(newSize, newSize, newSize);

        objekt.transform.localScale = size;

        float newVelocity = Randomizer.NextGaussian(mVelocity, stdVelocity);
        velocity = newVelocity;

        objekt.name = "Z" + iden.ToString();
        id = iden;

        var renderer = objekt.transform.GetChild(0).GetComponent<Renderer>();
        Color c = new Color(255f / 255, 255f / 255, 255f / 255);
        renderer.material.SetColor("_Color", c);
    }



    public void update(float dt)
    {

        time += dt;

        
            if (objekt.transform.localPosition.x > -10 && objekt.transform.localPosition.x < 10 && objekt.transform.localPosition.z > -10 && objekt.transform.localPosition.z < 10)
            {



                
                float rand = Random.Range(-1f, 1f);
                float rot = rand * 35f;
                if (Random.value < turningRate)
                {
                    objekt.transform.Rotate(new Vector3(0, rot, 0), Space.World);

                }

                position = position + objekt.transform.forward * velocity * dt;
                objekt.transform.localPosition = position;

    
            }
            else
            {
                if (objekt.transform.localPosition.x <= -10)
                {
                    objekt.transform.localRotation = Quaternion.Euler(0, 90, 0);
                    position.x = -10;
                }
                if (objekt.transform.localPosition.x >= 10)
                {
                    objekt.transform.localRotation = Quaternion.Euler(0, 270, 0);
                    position.x = 10;
                }
                if (objekt.transform.localPosition.z <= -10)
                {
                    objekt.transform.localRotation = Quaternion.Euler(0, 0, 0);
                    position.z = -10;
                }
                if (objekt.transform.localPosition.z >= 10)
                {
                    objekt.transform.localRotation = Quaternion.Euler(0, 180, 0);
                    position.z = 10;
                }
                objekt.transform.localPosition = position;


                position = position + objekt.transform.forward;
                objekt.transform.localPosition = position;



            }


    }




}

